import unittest
import uuid

from schemas import messages_pb2 as Message


class TestSchemas(unittest.TestCase):
    def test_step(self):
        name = uuid.uuid4().__str__()
        depends = [uuid.uuid4().__str__()] * 10
        step = Message.Step(name=name, depends=depends)
        unittest.TestCase().assertEqual(name, step.name)
        unittest.TestCase().assertEqual(depends, step.depends)

    def test_graph_message(self):
        taskid = uuid.uuid4().__str__()
        print(taskid, type(taskid))
        graphmsg = Message.GraphMessage(taskid=taskid)
        unittest.TestCase().assertEqual(taskid, graphmsg.taskid)


if __name__ == "__main__":
    unittest.main()
