# download protoc compiler
PROTOC_FILE=./protoc/bin/protoc
if [ ! -f "$PROTOC_FILE" ]; then
    wget https://github.com/protocolbuffers/protobuf/releases/download/v21.5/protoc-21.5-linux-aarch_64.zip
    unzip protoc-21.5-linux-aarch_64.zip -d protoc
    rm protoc-21.5-linux-aarch_64.zip
fi
$PROTOC_FILE --python_out=./src ./schemas/messages.proto