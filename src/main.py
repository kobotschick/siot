import json
import multiprocessing
import uuid
from locale import AM_STR

import pika
import yaml
from loguru import logger

import schemas.messages_pb2 as Message
from mathtree import get_steps_for_expr
from Node import Node

RABBITMQ_URL = "amqp://guest:guest@192.168.178.43:15672"
GRAPH_FILE = "graph.yaml"
exercises = [([1, "+", 2, "*", 4, "/", 3], 4)]

# def create_graph(excerise):
#   graph = {}
#   for e in excerise:
#      if e in "+-*/":
#          graph.update({prev_elem: {"next":prev_elem}})


def init_nodes(node_list):
    for n in node_list:
        node = Node(name=n, function=n)
        node.connect(RABBITMQ_URL)
        proc = multiprocessing.Process(target=node.run)
        proc.start()


def init_connections():
    channel.exchange_declare(exchange="logs", exchange_type="fanout")


def close_nodes():
    # fan out close signal to all nodes
    pass


def read_graph(filepath: str):
    with open(filepath, "r") as stream:
        try:
            parsed_yaml = yaml.safe_load(stream)
            logger.debug(parsed_yaml)
        except yaml.YAMLError as exc:
            logger.error(exc)
    return parsed_yaml


def create_graph_message(taskid, graph):
    steps_mapping = {}
    for i, step in enumerate(graph["steps"]):
        steps_mapping.update({step["name"]: i})

    def get_next_steps(cur_step):
        next_steps = []
        for i, step in enumerate(graph["steps"]):
            if cur_step in step.get("depends", []):
                next_steps.append(i)
        return next_steps

    msg_step_list = []
    for i, step in enumerate(graph["steps"]):
        params = {}
        for elem in step.get("params", []):
            params.update(elem)
        msg_step = Message.Step(
            name=step["name"],
            id=i,
            command=step["command"],
            dependsOn=[steps_mapping[n] for n in step.get("depends", [])],
            nextSteps=get_next_steps(step["name"]),
            params=params,
        )
        msg_step_list.append(msg_step)

    return Message.GraphMessage(taskId=taskid, stepId=0, steps=msg_step_list)


def trigger_graph(msg_graph):
    params = pika.URLParameters(RABBITMQ_URL)
    params.socket_timeout = 5

    connection = pika.BlockingConnection(params)  # Connect to CloudAMQP
    channel = connection.channel()  # start a channel
    channel.queue_declare(queue="central_queue")  # Declare a queue
    routing_key = "queue_+"
    channel.basic_publish(
        exchange="", routing_key=routing_key, body=msg_graph.SerializeToString()
    )
    logger.debug(f"main published on queue {routing_key}")
    connection.close()


def create_graph_from_expr(expr):
    (steps, dictn, result) = get_steps_for_expr(expr)
    graph = {}
    graph_steps = []
    for step in steps:

        if step[1].endswith("_Leaf"):
            a = dictn[step[1]]
        else:
            a = step[1]
        if step[2].endswith("_Leaf"):
            b = dictn[step[2]]
        else:
            b = step[2]
        graph_steps.append(
            {
                "name": step[0],
                "command": dictn[step[0]],
                "params": {"a": a, "b": b},
                "depens": depends,
            }
        )


def main():
    init_nodes(["+", "-", "*", "/"])
    graph = read_graph(GRAPH_FILE)
    print(type(graph))
    # msg_graph = create_graph_message(uuid.uuid4().__str__(), graph)
    # print(msg_graph.steps)
    # trigger_graph(msg_graph)
    close_nodes()


main()
