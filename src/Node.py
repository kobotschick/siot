import datetime
import json

import pika
from loguru import logger

import schemas.messages_pb2 as Message


def func_add(a, b):
    return a + b


def func_sub(a, b):
    return a - b


def func_mult(a, b):
    return a * b


def func_div(a, b):
    return a / b


func_dict = {"+": func_add, "-": func_sub, "*": func_mult, "/": func_div}


class Node:
    def __init__(self, name, function):
        self.name = name
        self.func = func_dict[function]
        self.connection = None
        self.channel = None
        self.input_channel = None
        self.input_queue = f"queue_{self.name}"
        logger.debug(f"Init Node done {self.input_queue=}")

    def connect(self, url):
        params = pika.URLParameters(url)
        params.socket_timeout = 5
        self.connection = pika.BlockingConnection(params)
        self.channel = self.connection.channel()  # start a channel
        self.input_channel = self.connection.channel()  # start a channel
        self.input_channel.queue_declare(
            queue=self.input_queue
        )  # Declare a input queue

    def disconnect(self):
        self.connection.close()

    def __publish(self, routing_key, msg):
        self.channel.basic_publish(exchange="", routing_key=routing_key, body=msg)
        logger.debug(f"{self.name} published on queue {routing_key}")

    def __input_callback(self, msg):
        def get_step(name):
            for step in msg.steps:
                if step.name == name:
                    return step
        cur_step = msg.steps[msg.stepId]
        a = cur_step.params.get("a")
        b = cur_step.params.get("b")
        next_steps_ids = cur_step.nextSteps
        for next_step_id in next_steps_ids:
            next_step = msg.steps[next_step_id]
            routing_key = f"queue_{next_step.command}"
            
            # b = float(d["b"])
            # c = self.func(a, b)

            msg.stepId = next_step_id
            self.__publish(routing_key, msg.SerializeToString())

    def __consume(self):
        def callback(ch, method, properties, body):
            msg = Message.GraphMessage()
            msg.ParseFromString(body)
            logger.debug(
                f"Got something on {self.input_queue}, {ch=}, {method=}, {properties=}, {msg=}"
            )
            self.__input_callback(msg)

        # set up subscription on the queue
        self.input_channel.basic_consume(self.input_queue, callback, auto_ack=True)

    def heartbeat(self):
        ts = datetime.datetime.now()
        self.__publish("central_queue", f"{ts} : {self.name} alive")

    def run(self):
        self.heartbeat()
        self.__consume()
        # start consuming (blocks)
        # self.input_channel.basic_consume(self.input_queue, self.callback2, auto_ack=True)
        self.input_channel.start_consuming()
        self.disconnect()
        logger.debug(f"{self.name} finished")
