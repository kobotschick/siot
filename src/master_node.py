import enum

from Node import Node

enum = {
    "NODE_DISCONNECT": 0,
}


class MasterNode(Node):
    def setup_channes(self):
        self.channel.exchange_declare(exchange="logs", exchange_type="fanout")
        message = " ".join(sys.argv[1:]) or "info: Hello World!"
        self.channel.basic_publish(exchange="logs", routing_key="", body=message)

    def close_all_Nodes(self):
        self.channel.basic_publish(exchange="central", routing_key="", body=message)
