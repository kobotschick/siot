import operator

from pythonds.basic.stack import Stack
from pythonds.trees.binaryTree import BinaryTree


def buildParseTree(fpexp):
    fplist = fpexp.split()
    pStack = Stack()
    eTree = BinaryTree("")
    pStack.push(eTree)
    currentTree = eTree

    for i in fplist:
        if i == "(":
            currentTree.insertLeft("")
            pStack.push(currentTree)
            currentTree = currentTree.getLeftChild()

        elif i in ["+", "-", "*", "/"]:
            currentTree.setRootVal(i)
            currentTree.insertRight("")
            pStack.push(currentTree)
            currentTree = currentTree.getRightChild()

        elif i == ")":
            currentTree = pStack.pop()

        elif i not in ["+", "-", "*", "/", ")"]:
            try:
                currentTree.setRootVal(int(i))
                parent = pStack.pop()
                currentTree = parent

            except ValueError:
                raise ValueError("token '{}' is not a valid integer".format(i))

    return eTree


def postordereval(tree):
    opers = {
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "/": operator.truediv,
    }
    res1 = None
    res2 = None
    if tree:
        res1 = postordereval(tree.getLeftChild())
        res2 = postordereval(tree.getRightChild())
        if res1 and res2:
            return opers[tree.getRootVal()](res1, res2)
        else:
            return tree.getRootVal()


def get_steps_for_expr(expr):
    pt = buildParseTree(expr)

    steps = []
    dictn = {}

    def postorder(tree, id="I"):
        if tree is not None:
            left_child_id = f"{id}L"
            right_child_id = f"{id}R"
            has_left_child = postorder(tree.getLeftChild(), left_child_id)
            has_right_child = postorder(tree.getRightChild(), right_child_id)
            # print(id, tree.getRootVal(), hasLeftChild & hasRightChild)

            if has_left_child & has_right_child:
                steps.append((id, left_child_id, right_child_id))
            else:
                id = f"{id}_Leaf"
            dictn.update({id: tree.getRootVal()})
            return True
        return False

    postorder(pt)  # defined and explained in the next section
    result = postordereval(pt)
    return (steps, dictn, result)


if __name__ == "__main__":
    (steps, dictn, result) = get_steps_for_expr(
        "( ( 10 + 5 ) * ( 1 + 3 ) * ( 2 + 4 * 5 * ( 1 + 2 ) ) )"
    )
    print((steps, dictn, result))
